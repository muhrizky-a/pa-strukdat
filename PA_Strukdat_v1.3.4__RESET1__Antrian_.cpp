#include <iostream>

/* Baca / Tulis file txt */
#include <fstream>

//Untuk menggunakan fungsi sleep()
#include <unistd.h>

//Untuk menggunakan fungsi getch()
#include <conio.h>

//Untuk menggunakan fungsi setw();
#include <iomanip>

//Untuk atoi
#include <stdlib.h>

//untuk deteksi waktu kapan pembelian barang
#include <ctime>

//Untuk convert angka ke string
#include <sstream>

#define delay Sleep

//untuk fungsi gotoxy
#include <windows.h>
using namespace std;


void MainMenu();

COORD coord = {X: 0, Y: 0};
void gotoxy(int x, int y){
	coord.X = x;
	coord.Y = y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), coord);
}


void Opening(){
	int x_pos = 33;
	gotoxy(x_pos,5); 	cout<<"||--------------------------------------------------------||"<<endl;
	gotoxy(x_pos,6);	cout<<"||--------------------------------------------------------||"<<endl;
	gotoxy(x_pos,7);	cout<<"||                     .|''''                             ||"<<endl;
	gotoxy(x_pos,8);	cout<<"||                       | |                              ||"<<endl;
	gotoxy(x_pos,9);	cout<<"||                       |'|            ._____            ||"<<endl;
	gotoxy(x_pos,10);	cout<<"||               ___    |  |            |.   |' .---''    ||"<<endl;
	gotoxy(x_pos,11);	cout<<"||       _    .-'   '-. |  |     .--'|  ||   | _|    |    ||"<<endl;
	gotoxy(x_pos,12);	cout<<"||    .-'|  _.|  |    ||   '-__  |   |  |    ||      |    ||"<<endl;
	gotoxy(x_pos,13);	cout<<"||    |' | |.    |    ||       | |   |  |    ||      |    ||"<<endl;
	gotoxy(x_pos,14);	cout<<"||____|  '-'     '    ''       '-'   '-.'    '`      |____||"<<endl;
	gotoxy(x_pos,15);	cout<<"||~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~||"<<endl;
	gotoxy(x_pos,16); 	cout<<"||--------------------------------------------------------||"<<endl;
	for(int i=x_pos-1;i<61; i++){
		gotoxy(i,17);	cout <<"=================================";
		gotoxy(i,18);	cout <<"| SELAMAT DATANG DI K-BANG MART |";
		gotoxy(i,19);	cout <<"=================================";
		delay(75);
		gotoxy(i,17);cout << " ";
		gotoxy(i,18);cout << " ";
		gotoxy(i,19);cout << " ";
		
    }
}

void hiasan(){
	system("color 17");
	char a=177, b=219;
	int timing = 10;
	
	int atas_bawah =30, kiri_kanan = 119;
	
	//atas
	for (int i=0;i<kiri_kanan; i++){
		gotoxy(i,0);
		cout << b;
		Sleep(timing);
 	}
 	
 	//bawah
	for (int i=kiri_kanan-1; i>=0; i--){
		gotoxy(i,atas_bawah-1);
		cout << b;
		Sleep(timing);
 	}
 	
 	//kanan
	for (int i=0;i<atas_bawah; i++){
		gotoxy(kiri_kanan,i);
		cout << b;
		Sleep(timing);
	}
	
 	
 	//kiri
	for (int i=atas_bawah-1; i>= 0 ; i--){
		gotoxy(0,i);
		cout << b;
		Sleep(timing);
 	}
}


void hiasan1(){
	system("color 17");
	char a=177, b=219;
	
	int atas_bawah =30, kiri_kanan = 119;
	
	//atas
	for (int i=0;i<kiri_kanan; i++){
		gotoxy(i,0);
		cout << b;
 	}
 	
 	//kanan
	for (int i=0;i<atas_bawah; i++){
		gotoxy(kiri_kanan,i);
		cout << b;
	}
	
	//bawah
	for (int i=kiri_kanan-1; i>=0; i--){
		gotoxy(i,atas_bawah-1);
		cout << b;
 	}
 	
 	//kiri
	for (int i=atas_bawah-1; i>= 0 ; i--){
		gotoxy(0,i);
		cout << b;
 	}
}


struct produk{
	string nama;
	int harga, stok;
	produk* next;
	produk* prev;
};

struct keranjang{
	string nama;
	int jumlah, harga;
	keranjang* next;
};

struct pembeli{
	string nama, noAntrian, metode_pembayaran;
	int jumlah, harga, jam, menit, hari, bulan, tahun;
	pembeli* next;
	pembeli* prev;
	keranjang* HeadKrj = NULL;
};

//Mewakili antrian pembeli
pembeli *HEAD_Antrian = NULL, *TAIL_Antrian = NULL;

//Berisi daftar riwayat pembeli
pembeli *HEAD_RiwayatPembeli = NULL, *TAIL_RiwayatPembeli = NULL;

produk *HEAD_Makanan=NULL, 			*TAIL_Makanan = NULL;
produk *HEAD_Minuman=NULL, 			*TAIL_Minuman = NULL;
produk *HEAD_Obat=NULL, 			*TAIL_Obat = NULL;
produk *HEAD_ATK=NULL, 				*TAIL_ATK = NULL;
produk *HEAD_Elektronik=NULL, 		*TAIL_Elektronik = NULL;
produk *HEAD_Perabot=NULL, 			*TAIL_Perabot = NULL;
produk *HEAD_PeralatanBayi=NULL, 	*TAIL_PeralatanBayi = NULL;

bool sortNama_Makanan = 0, sortNama_Minuman = 0, sortNama_Obat = 0,sortNama_ATK = 0,sortNama_Perabot = 0, sortNama_Elektronik = 0, sortNama_PltBayi = 0;
int reset, CURR_Antrian = 1, CURR_Antrian_TEMP = 1;




void insertKeranjang(keranjang* x, pembeli* HEAD){
	if(HEAD->HeadKrj == NULL){
		HEAD->HeadKrj = x;
	} else {
		keranjang* pointer = HEAD->HeadKrj;
		while( pointer->next != NULL){
			pointer=pointer->next;
		}
		pointer->next = x;
	}
}
bool subHapusKeranjang(keranjang* ptrKrjg, produk* HEADProduk, string namaCari){
	produk* ptrProduk = HEADProduk;
	while(ptrProduk != NULL){
		if ( namaCari == ptrProduk->nama){
			ptrProduk->stok +=ptrKrjg->jumlah;	
			return true;
		}
		ptrProduk = ptrProduk->next;
	}
	return false;
}
void hapusKeranjang(pembeli* pembeli1, string nama){
	keranjang* ptrKrjg = pembeli1->HeadKrj;
	produk ptrProduk;
	bool statusHapus = false;
	
	
	while(ptrKrjg != NULL && statusHapus ==false){

		statusHapus = subHapusKeranjang(ptrKrjg, HEAD_Makanan, nama);
		statusHapus = subHapusKeranjang(ptrKrjg, HEAD_Minuman, nama);
		statusHapus = subHapusKeranjang(ptrKrjg, HEAD_Obat, nama);
		statusHapus = subHapusKeranjang(ptrKrjg, HEAD_ATK, nama);
		statusHapus = subHapusKeranjang(ptrKrjg, HEAD_Perabot, nama);
		statusHapus = subHapusKeranjang(ptrKrjg, HEAD_Elektronik, nama);
		statusHapus = subHapusKeranjang(ptrKrjg, HEAD_PeralatanBayi, nama);
		
		ptrKrjg = ptrKrjg->next;
	}
	
}
void deleteFirst(keranjang** HEAD){
	keranjang* temp = (*HEAD)->next;
	(*HEAD) = NULL;
	(*HEAD) = temp;
}
void deleteLast(keranjang** HEAD){
	keranjang* pointer = (*HEAD);
	if( (*HEAD)->next == NULL){
		(*HEAD) = NULL;
	} else{
		while( pointer->next != NULL){
			pointer=pointer->next;
		}
		pointer = NULL;
	}
}

void deleteAt(int indeks, keranjang** HEAD){
	keranjang* pointer = (*HEAD);
	keranjang* temp;
	
	for(int i=1; i < indeks-1; i++){
		pointer = pointer->next;
	}
	temp=pointer->next;
	pointer->next=temp->next;
	temp = NULL;
}




void insertDoubleLast(produk* x,produk** HEAD, produk** TAIL){
	if(*HEAD == NULL){
		(*HEAD) = x;
		(*TAIL) = x;
	} else {
		x->prev = (*TAIL);
		(*TAIL)->next = x;
		(*TAIL) = x;
	}
}
void deleteDoubleFirst(produk** HEAD, produk** TAIL){
	if( (*HEAD) == (*TAIL)){
		(*HEAD) = NULL;
	}else{
		produk* temp = (*HEAD);
		(*HEAD) = (*HEAD)->next;
		(*HEAD)->prev = NULL;
		temp->next = NULL;
		temp = NULL;
		delete temp;	
	}
}

void deleteDoubleLast(produk** HEAD, produk** TAIL){
	produk* pointer = (*HEAD);
	
	if( (*HEAD) == (*TAIL)){
		deleteDoubleFirst(HEAD, TAIL);
	} else{
		produk* temp = (*TAIL);
		(*TAIL) = (*TAIL)->prev;
		(*TAIL)->next = NULL;
		
		temp->next = NULL;
		temp->prev = NULL;
		temp=NULL;
		delete temp;
	}
}

void deleteDoubleAt(int indeks,produk** HEAD){
	produk* pointer = (*HEAD);
	produk* temp;
	for(int i=1; i < indeks-1; i++){
		pointer = pointer->next;
	}
	temp=pointer->next;
	pointer->next=temp->next;
	(temp->next)->prev = pointer;
	
	temp = NULL;
	delete temp;
}

void subTambahStokKembali(keranjang* ptrKrjg, produk* HEADProduk){
	produk* ptrProduk = HEADProduk;
	while(ptrProduk != NULL){
		if (ptrKrjg->nama == ptrProduk->nama){
			ptrProduk->stok +=ptrKrjg->jumlah;
			break;
		}
		ptrProduk = ptrProduk->next;
	}
}
void TambahStokKembali(pembeli* pembeli1){
	keranjang* ptrKrjg = pembeli1->HeadKrj;
	produk ptrProduk;
	while(ptrKrjg != NULL){
		subTambahStokKembali(ptrKrjg, HEAD_Makanan);
		subTambahStokKembali(ptrKrjg, HEAD_Minuman);
		subTambahStokKembali(ptrKrjg, HEAD_Obat);
		subTambahStokKembali(ptrKrjg, HEAD_ATK);
		subTambahStokKembali(ptrKrjg, HEAD_Perabot);
		subTambahStokKembali(ptrKrjg, HEAD_Elektronik);
		subTambahStokKembali(ptrKrjg, HEAD_PeralatanBayi);
		ptrKrjg = ptrKrjg->next;
	}
	
}

produk* getProduk(int indeks, string kategori){
	produk* pointer = (kategori == "makanan")	? HEAD_Makanan :
		(kategori == "minuman")					? HEAD_Minuman:
		(kategori == "obat")					? HEAD_Obat:
		(kategori == "atk")						? HEAD_ATK:
		(kategori == "perabot")					? HEAD_Perabot:
		(kategori == "elektronik")				? HEAD_Elektronik:
		(kategori == "peralatanbayi")			? HEAD_PeralatanBayi: NULL;
		
	for(int i=0; i < indeks-1; i++){
		pointer = pointer->next;
	}
	return pointer;
}


produk* updateProduk(int indeks,produk** HEAD){
	produk* pointer = (*HEAD);
	for(int i=0; i < indeks-1; i++){
		pointer = pointer->next;
	}
	return pointer;
}


pembeli* getFirstAntrian(){
	return HEAD_Antrian;
}

void enqueueRiwayatOrAntrian(pembeli* x, pembeli** HEAD, pembeli** TAIL){
	if((*HEAD) == NULL){
		(*HEAD) = x;
		(*TAIL) = x;
	} else {
		x->prev = (*TAIL);
		(*TAIL)->next = x;
		(*TAIL) = x;
	}
}
void dequeueRiwayatOrAntrian(pembeli** HEAD, pembeli** TAIL){
	if( (*HEAD) != NULL ){
		if( (*HEAD) == (*TAIL) ){
			(*HEAD) = NULL;
		}else{
			pembeli* temp = (*HEAD);
			(*HEAD) = (*HEAD)->next;
			(*HEAD)->prev = NULL;
			temp->next = NULL;
			temp = NULL;
			delete temp;	
		}
	}	
}

void dequeueAllRiwayat(){
	while(HEAD_RiwayatPembeli !=NULL) {
		dequeueRiwayatOrAntrian(&HEAD_RiwayatPembeli, &TAIL_RiwayatPembeli);
	}
}
void dequeueAllAntrian(){
	while(HEAD_Antrian !=NULL) {
		dequeueRiwayatOrAntrian(&HEAD_Antrian, &HEAD_Antrian);
	}
}

void DeteksiResetRiwayat(){
	time_t sekarang = time(0);
	struct tm *jam = localtime(&sekarang);
	
	int hari = jam-> tm_mday;
	int bulan = 1 + jam-> tm_mon;
	int tahun = 1900 + jam-> tm_year;

	static int t[] = { 0, 3, 2, 5, 0, 3, 
                       5, 1, 4, 6, 2, 4 };  
    tahun -= bulan < 3;  
    int day = ( tahun + tahun / 4 - tahun / 100 +  tahun / 400 + t[bulan - 1] + hari) % 7;  
    
    
    string hari_sekarang = 	day == 0? "Minggu":
		    				day == 1? "Senin":
		    				day == 2? "Selasa":
			    			day == 3? "Rabu":
		    				day == 4? "Kamis":
		   					day == 5? "Jum'at":
		    				day == 6? "Sabtu":"";
		    				
    if ( hari_sekarang == "Senin" && reset == 0) {
    	dequeueAllRiwayat();
    	dequeueAllAntrian();
		CURR_Antrian = 1;
		CURR_Antrian_TEMP = 1;
		reset = 1;	
		
	} else if ( hari_sekarang != "Senin" ){
		reset = 0;
	}
}


void ReadProdukLoadout(string filename, produk** HEAD, produk** TAIL){
	//Fungsi baca file produk
	string ambil, direktori = "barang/" + filename + ".txt";
	int index=1, stok, harga;
	
	fstream file_produk;
	file_produk.open(direktori.c_str(), ios::in); 
	
	produk* produkBaru = new produk();
	while(getline(file_produk, ambil)){ 
		if(index % 4== 1 ){ 
			produkBaru->nama = ambil;
			
		} else if(index % 4== 2 ){ 
			stok = atoi(ambil.c_str()); 
			produkBaru->stok  = stok;
			
		} else if(index % 4 == 3){ 
			harga = atoi(ambil.c_str()); 
			produkBaru->harga  = harga;
			
			insertDoubleLast(produkBaru,HEAD,TAIL);
			produkBaru = new produk();
			
		}
		index++;
	}
	file_produk.close();
	
}

void Loadout(){
	//Berisi produk barang & riwayat pembelian yg di-load ke struct saat awal program
	string ambil;
	int index, jumlah, harga, jam, menit, hari, bulan, tahun;
	
	ReadProdukLoadout("file_makanan", &HEAD_Makanan, &TAIL_Makanan);
	ReadProdukLoadout("file_minuman", &HEAD_Minuman, &TAIL_Minuman);
	ReadProdukLoadout("file_obat", &HEAD_Obat, &TAIL_Obat);
	ReadProdukLoadout("file_atk", &HEAD_ATK, &TAIL_ATK);
	ReadProdukLoadout("file_perabot", &HEAD_Perabot, &TAIL_Perabot);
	ReadProdukLoadout("file_elektronik", &HEAD_Elektronik, &TAIL_Elektronik);
	ReadProdukLoadout("file_peralatanbayi", &HEAD_PeralatanBayi, &TAIL_PeralatanBayi);

	fstream file_riwayat_pembeli;
	fstream file_riwayat_barang;
	
	fstream file_antrian_pembeli;
	fstream file_antrian_barang;

	file_riwayat_pembeli.open		("riwayat/file_riwayat_pembeli.txt", ios::in);
	file_riwayat_barang.open		("riwayat/file_riwayat_barang.txt", ios::in);
	
	file_antrian_pembeli.open		("antrian/file_antrian_pembeli.txt", ios::in);
	file_antrian_barang.open		("antrian/file_antrian_barangpembeli.txt", ios::in);
	
	
	index = 1;
	pembeli* baru = new pembeli();
	while(getline(file_riwayat_pembeli, ambil)){ 
		
		if(index % 10 == 1 ){ 
			baru->noAntrian = ambil;
			
		} else if(index % 10 == 2 ){ 
			baru->nama = ambil;
			
		} else if(index % 10 == 3 ){ 
			jumlah = atoi(ambil.c_str()); 
			baru->jumlah = jumlah;
			
		} else if(index % 10 == 4){ 
			harga = atoi(ambil.c_str()); 
			baru->harga  = harga;
			
		} else if(index % 10 == 5){ 
			hari = atoi(ambil.c_str()); 
			baru->hari = hari;
			
		} else if(index % 10 == 6){ 
			bulan = atoi(ambil.c_str()); 
			baru->bulan = bulan;
			
		} else if(index % 10 == 7){ 
			tahun = atoi(ambil.c_str()); 
			baru->tahun = tahun;
			
		} else if(index % 10 == 8){ 
			jam = atoi(ambil.c_str()); 
			baru->jam = jam;
			
		} else if(index % 10 == 9){ 
			menit = atoi(ambil.c_str()); 
			baru->menit = menit;
			
		} else if(index % 10 == 0){ 
			reset = atoi(ambil.c_str()); 
			enqueueRiwayatOrAntrian(baru, &HEAD_RiwayatPembeli, &TAIL_RiwayatPembeli);
			baru = new pembeli();
	
		}  
		index++;
	}
	
	index = 1;
	keranjang* baruKrj = new keranjang();
	pembeli* ptrPembeli;

	while(getline(file_riwayat_barang, ambil)){
		if(ambil.find("=====INDEXBARU=====") != string::npos){
			ptrPembeli = (index == 1) ? HEAD_RiwayatPembeli : ptrPembeli->next;
			index++;
			
		} else if(ambil.find("Jumlah") != string::npos){ 
			jumlah = atoi(ambil.c_str()); 
			baruKrj->jumlah = jumlah;
			
		} else if(ambil.find("Harga") != string::npos){ 
			harga = atoi(ambil.c_str()); 
			baruKrj->harga = harga;
			if( ptrPembeli->HeadKrj == NULL ){
				ptrPembeli->HeadKrj = baruKrj;
				ptrPembeli->HeadKrj->next = NULL;
			}else{
				baruKrj->next = NULL;
				keranjang* ptr = ptrPembeli->HeadKrj;
				while(ptr->next != NULL){
					ptr=ptr->next;
				}
				ptr->next = baruKrj;
			}
			baruKrj = new keranjang();
			
		} else { 
			baruKrj->nama = ambil;
			
		}	
	} 
	
	
	
	
	
	
	
	
	index = 1;
	pembeli* antrianBaru = new pembeli();
	while(getline(file_antrian_pembeli, ambil)){ 
		
		if(index % 11 == 1 ){ 
			antrianBaru->noAntrian = ambil;
			
		} else if(index % 11 == 2 ){ 
			antrianBaru->nama = ambil;
			
		} else if(index % 11 == 3 ){ 
			antrianBaru->metode_pembayaran = ambil;
			
		} else if(index % 11 == 4 ){ 
			jumlah = atoi(ambil.c_str()); 
			antrianBaru->jumlah = jumlah;
			
		} else if(index % 11 == 5){ 
			harga = atoi(ambil.c_str()); 
			antrianBaru->harga  = harga;
			
		} else if(index % 11 == 6){ 
			hari = atoi(ambil.c_str()); 
			antrianBaru->hari = hari;
			
		} else if(index % 11 == 7){ 
			bulan = atoi(ambil.c_str()); 
			antrianBaru->bulan = bulan;
			
		} else if(index % 11 == 8){ 
			tahun = atoi(ambil.c_str()); 
			antrianBaru->tahun = tahun;
			
		} else if(index % 11 == 9){ 
			jam = atoi(ambil.c_str()); 
			antrianBaru->jam = jam;
			
		} else if(index % 11 == 10){ 
			menit = atoi(ambil.c_str()); 
			antrianBaru->menit = menit;
			
		} else if(index % 11 == 0){ 
			CURR_Antrian = atoi(ambil.c_str()); 
			CURR_Antrian_TEMP = atoi(ambil.c_str()); 
			
			enqueueRiwayatOrAntrian(antrianBaru, &HEAD_Antrian, &TAIL_Antrian);
			
			antrianBaru = new pembeli();
		}  
		index++;
	}
	
	index = 1;
	keranjang* antrianKrj = new keranjang();
	pembeli* ptrAntrian;

	while(getline(file_antrian_barang, ambil)){
		if(ambil.find("=====INDEXBARU=====") != string::npos){
			ptrAntrian = (index == 1) ? HEAD_Antrian : ptrAntrian->next;
			index++;
			
		} else if(ambil.find("Jumlah") != string::npos){ 
			jumlah = atoi(ambil.c_str()); 
			antrianKrj->jumlah = jumlah;
			
		} else if(ambil.find("Harga") != string::npos){ 
			harga = atoi(ambil.c_str()); 
			antrianKrj->harga = harga;
			if( ptrAntrian->HeadKrj == NULL ){
				ptrAntrian->HeadKrj = antrianKrj;
				ptrAntrian->HeadKrj->next = NULL;
			}else{
				antrianKrj->next = NULL;
				keranjang* ptr = ptrAntrian->HeadKrj;
				while(ptr->next != NULL){
					ptr=ptr->next;
				}
				ptr->next = antrianKrj;
			}
			
			antrianKrj = new keranjang();
			
		} else { 
			antrianKrj->nama = ambil;
			
		}	
	} 

	file_riwayat_pembeli.close();
	file_riwayat_barang.close();
	
	file_antrian_pembeli.close();
	file_antrian_barang.close();
	
}


void SaveProdukFunction(string filename, produk* HEAD){
	//Fungsi baca file produk
	string ambil, direktori = "barang/" + filename + ".txt";
	int index=1, stok, harga;
	
	fstream file_produk;
	file_produk.open(direktori.c_str(), ios::out | ios::trunc); 
	
	produk* ptr = HEAD; 
    while (ptr != NULL) { 
    	file_produk << ptr->nama  <<endl;
		file_produk << ptr->stok << "  stok" <<endl;
		file_produk << ptr->harga  << " harga" <<endl;
		file_produk << "=====[Batas Index]=====" <<endl;
        ptr = ptr->next; 
    } 
	file_produk.close();
}

void SaveDataOnExit(){
	//Simpan data produk & riwayat sebelum logout ( Admin )
	
	SaveProdukFunction("file_makanan", HEAD_Makanan);
	SaveProdukFunction("file_minuman", HEAD_Minuman);
	SaveProdukFunction("file_obat", HEAD_Obat);
	SaveProdukFunction("file_atk", HEAD_ATK);
	SaveProdukFunction("file_perabot", HEAD_Perabot);
	SaveProdukFunction("file_elektronik", HEAD_Elektronik);
	SaveProdukFunction("file_peralatanbayi", HEAD_PeralatanBayi);
	
	
	fstream file_riwayat_pembeli;
	fstream file_riwayat_barang;
	fstream file_antrian_pembeli;
	fstream file_antrian_barang;

	file_antrian_pembeli.open		("antrian/file_antrian_pembeli.txt", ios::out | ios::trunc);
	file_antrian_pembeli.open		("antrian/file_antrian_barangpembeli.txt", ios::out | ios::trunc);
	
	file_riwayat_pembeli.open		("riwayat/file_riwayat_pembeli.txt", ios::out | ios::trunc);
	file_riwayat_barang.open		("riwayat/file_riwayat_barang.txt", ios::out | ios::trunc);
	
	pembeli* ptr = HEAD_RiwayatPembeli;
	keranjang* ptrKeranjang; 
    while (ptr != NULL) { 
    	file_riwayat_pembeli << ptr->noAntrian  <<endl;
		file_riwayat_pembeli << ptr->nama  <<endl;
		file_riwayat_pembeli << ptr->jumlah << " : [Jumlah Barang]" <<endl;
		file_riwayat_pembeli << ptr->harga  <<endl;
		file_riwayat_pembeli << ptr->hari  <<endl;
		file_riwayat_pembeli << ptr->bulan  <<endl;
		file_riwayat_pembeli << ptr->tahun  <<endl;
		file_riwayat_pembeli << ptr->jam  <<endl;
		file_riwayat_pembeli << ptr->menit <<endl;
		file_riwayat_pembeli << reset << " : Keadaan Reset=====[Batas Index]=====" <<endl;
		
		file_riwayat_barang << "=====INDEXBARU=====" << endl;
		ptrKeranjang = ptr->HeadKrj;
		while (ptrKeranjang != NULL) {
			file_riwayat_barang << ptrKeranjang->nama << endl;
			file_riwayat_barang << ptrKeranjang->jumlah << " Jumlah" << endl;
			file_riwayat_barang << ptrKeranjang->harga << " Harga" << endl;
			ptrKeranjang = ptrKeranjang->next;
		}
		
        ptr = ptr->next; 
    } 
    
    ptr = HEAD_Antrian;
    while (ptr != NULL) { 
    	file_antrian_pembeli << ptr->noAntrian  <<endl;
		file_antrian_pembeli << ptr->nama  <<endl;
		file_antrian_pembeli << ptr->metode_pembayaran  <<endl;
		file_antrian_pembeli << ptr->jumlah << " : [Jumlah Barang]" <<endl;
		file_antrian_pembeli << ptr->harga  <<endl;
		file_antrian_pembeli << ptr->hari  <<endl;
		file_antrian_pembeli << ptr->bulan  <<endl;
		file_antrian_pembeli << ptr->tahun  <<endl;
		file_antrian_pembeli << ptr->jam  <<endl;
		file_antrian_pembeli << ptr->menit <<endl;
		file_antrian_pembeli << CURR_Antrian << " : [Nomor Antrian Terakhir]=====[Batas Index]=====" <<endl;
		
		file_antrian_barang << "=====INDEXBARU=====" << endl;
		ptrKeranjang = ptr->HeadKrj;
		while (ptrKeranjang != NULL) {
			file_antrian_barang << ptrKeranjang->nama << endl;
			file_antrian_barang << ptrKeranjang->jumlah << " Jumlah" << endl;
			file_antrian_barang << ptrKeranjang->harga << " Harga" << endl;
			ptrKeranjang = ptrKeranjang->next;
		}
		
        ptr = ptr->next; 
    } 

	file_riwayat_pembeli.close();
	file_riwayat_barang.close();
	
	file_antrian_pembeli.close();
	file_antrian_barang.close();
}



int min(int x, int y) { 
	return (x<=y)? x : y; 
	} 
  

int fibonacciSearch(produk arr[], string x, int n){ 
    // Inisialisasi angka fibonacci yang ke-0 dan ke-1
    int fib_m2 = 0;   // Fibonacci Fm-2
    int fib_m1 = 1;   // Fibonacci Fm-1
    int fib_m = fib_m2 + fib_m1;  // Fibonacci yang ke - m 
  
    // Generate angka fibonacci hingga > n
    while (fib_m < n) { 
        fib_m2 = fib_m1; 
        fib_m1 = fib_m; 
        fib_m  = fib_m2 + fib_m1; 

    } 
  
    // Penanda batas pencarian
    int batas = -1; 
  
    // Angka yang ingin dicari akan diperiksa di tiap arr [indeks] hingga fib_m sisa 1, yang berarti fib_n2 = 0 dan fib_n1 = 1
    
    while (fib_m > 1) { 	
        // Menentukan index berdasarkan angka fibonacci
        int i = min(batas+fib_m2, n-1); 
    	//Jika angka x lebih besar daripada arr[i], maka array dari batas ke index i diabaikan, Fm menjadi F (m-1)
        if (arr[i].nama < x) { 
            fib_m  = fib_m1; 
            fib_m1 = fib_m2; 
            fib_m2 = fib_m - fib_m1; 
            batas = i; 
        } 
  	
        //Jika angka x lebih kecil daripada arr[i], maka array setelah index i diabaikan, Fm menjadi F (m-2)
        else if (arr[i].nama > x) { 
            fib_m  = fib_m2; 
            fib_m1 = fib_m1 - fib_m2; 
            fib_m2 = fib_m - fib_m1; 
        } 
        
		// Jika ketemu, kembalikan nilai index
		else {	
			
        	return i;
		}
    } 
    
    // Kembalikan nilai index array dari bilangan yang dicari jika ditemukan
    
	if(batas+1 < n){
		if ( fib_m1 && arr[batas+1].nama == x ){
	    	return batas+1; 
		} 
	}

	return -1; 
}

//QUICK SORT
void quickPartitionSwap(produk* x, produk* y){
	produk temp = *x;
	*x = *y;
	*y = temp;
}

int partisi( produk data[], string jenis, int low, int high, int urutan){
	int i = (low - 1);
	
	//pivot atau nlai acuan dalam quick sort
	if(jenis == "nama"){
		string pivot = data[high].nama;
		for (int j = low; j <= high - 1; j++){
			if(urutan == 1){
				if (data[j].nama < pivot){
					i++;
					quickPartitionSwap(&data[i], &data[j]);
				}
			} else if(urutan == 2){
				if (data[j].nama > pivot){
					i++;
					quickPartitionSwap(&data[i], &data[j]);
				}
			}	
		}
	} else if (jenis == "harga"){
		int pivot = data[high].harga;
		for (int j = low; j <= high - 1; j++){
			if(urutan == 1){
				if (data[j].harga < pivot){
					i++;
					quickPartitionSwap(&data[i], &data[j]);
				}
			} else if(urutan == 2){
				if (data[j].harga > pivot){
					i++;
					quickPartitionSwap(&data[i], &data[j]);
				}
			}	
		}
	}

	quickPartitionSwap(&data[i +1], &data[high]);
	return (i + 1);
}

void quickSort(produk data[], string jenis, int low, int high, int urutan){
	if (low < high){
		int pi = partisi(data, jenis, low, high, urutan);
		
		quickSort(data, jenis, low, pi - 1, urutan);
		quickSort(data, jenis, pi + 1, high, urutan);
	}

}


void assignLinkedListToArray(produk* HEAD, produk* TAIL, produk data[]){
	produk* pointer = HEAD;
	
	int i = 0;
	while(pointer != TAIL->next){
		data[i] = *pointer;
		i++;
		pointer = pointer->next;
	}
}

void assignArrayToLinkedList(produk** HEAD, produk** TAIL, produk data[]){
	int i = 0;
	(*HEAD)->nama = data[i].nama;
	(*HEAD)->stok = data[i].stok;
	(*HEAD)->harga = data[i].harga;
	produk *pointer = (*HEAD)->next;
	i++;
	
	while( pointer != (*TAIL)->next){
		pointer->nama = data[i].nama;
		pointer->stok = data[i].stok;
		pointer->harga = data[i].harga;
		
		i++;
		pointer = pointer->next;
		
	}
}


int length(produk* head){ 
    produk* curr = head; 
    int count = 0; 
    while (curr != NULL) { 
        count++; 
        curr = curr->next; 
    } 
    return count; 
} 



void MessageLoading( string pesan, int titik){
	system("cls");
	string titik_loading =  (titik == 1 ) ? ".   ":
							(titik == 2 ) ? "..  ":
							(titik == 3 ) ? "... ":
							(titik == 4 ) ? "....":"";
	gotoxy (23,14); cout << "+"<< string(70, '=') << "+" << endl;
	if ( pesan == "login" ){
		gotoxy (23,15); cout << "|                       Autentikasi Login Admin" << titik_loading << "                    |" << endl;
	}
	
	else if ( pesan == "tambah" ){
		gotoxy (23,15); cout << "|                          Menambahkan Produk" << titik_loading << "                      |" << endl;
	}
	else if ( pesan == "cari" ){
		gotoxy (23,15); cout << "|                            Mencari Produk" << titik_loading << "                        |" << endl;
	}
	else if ( pesan == "ubah" ){
		gotoxy (23,15); cout << "|                         Mengubah Data Produk" << titik_loading << "                     |" << endl;
	}
	else if ( pesan == "hapus" ){
		gotoxy (23,15); cout << "|                           Menghapus Produk" << titik_loading << "                       |" << endl;
	}
	else if ( pesan == "exit" ){
		gotoxy (23,15); cout << "|                         Keluar Dari Aplikasi" << titik_loading << "                     |" << endl;
	}
	
	 
	gotoxy (23,16); cout << "+"<< string(70, '=') << "+" << endl;
}

void Loading(string pesan){
	for ( int i = 0; i < 3; i++){
		MessageLoading(pesan,1);
		for ( int j = 0; j < 1e8 ; j++){}
		MessageLoading(pesan,2);
		for ( int j = 0; j < 1e8 ; j++){}
		MessageLoading(pesan,3);
		for ( int j = 0; j < 1e8 ; j++){}
		MessageLoading(pesan,4);
		for ( int j = 0; j < 1e8 ; j++){}
	}	
}

void Message(int a){
	system("cls");
	gotoxy (23,13); cout << "+"<< string(70, '=') << "+" << endl;
	gotoxy (23,14); cout << "|                                                                      |" << endl;
	if (a==1){
		gotoxy (23,15); cout << "|              Maaf pilihan yang anda masukkan tidak benar             |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
		
	} else if (a==3){
		gotoxy (23,15); cout << "|                      Pembelian Barang Dibatalkan                     |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
		
	} else if (a==4){
		gotoxy (23,15); cout << "|              Identitas tidak cocok dengan data pada ATM!             |" << endl;
		gotoxy (23,16); cout << "|                      Pembelian Barang Dibatalkan                     |" << endl;
		
	} else if (a==5){
		gotoxy (23,15); cout << "|                         Saldo ATM tidak cukup                        |" << endl;
		gotoxy (23,16); cout << "|                      Pembelian Barang Dibatalkan                     |" << endl;
		
	} else if (a==6){
		gotoxy (23,15); cout << "|                   Alat Pembayaran tidak terdeteksi!                  |" << endl;
		gotoxy (23,16); cout << "|                      Pembelian Barang Dibatalkan                     |" << endl;
		
	} else if (a == 7){
		gotoxy (23,15); cout << "|              Produk telah dimasukkan ke Keranjang Anda.              |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
		
	}
	
	
	//Pesan Create
	else if (a == 10){
		gotoxy (23,15); cout << "|                      Penambahan produk Berhasil                      |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
	}
	else if (a == 11){
		gotoxy (23,15); cout << "|                                ERROR!                                |" << endl;
		gotoxy (23,16); cout << "|             Input Harga salah / tidak dapat dibawah Rp. 1            |" << endl;
	}
	else if (a == 12){
		gotoxy (23,15); cout << "|                          Input Stok salah!!                          |" << endl;
		gotoxy (23,16); cout << "|                      Range input stok: 1 - 9999                      |" << endl;
	}
	
	//Pesan Update
	else if (a == 20){
		gotoxy (23,15); cout << "|                      Data produk berhasil diubah                     |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
	} 
	//Pesan Delete
	else if (a == 30){
		gotoxy (23,15); cout << "|                      Penghapusan produk Berhasil                     |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
	}
	
	//Pesan admin
	
	else if (a == 77){
		gotoxy (23,15); cout << "|                     Produk yang dicari tidak ada                     |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
		
	}
	
	//Login
	else if (a==88){
		gotoxy (23,15); cout << "|                         Password anda salah                          |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
		
	} else if (a==89){
		gotoxy (23,15); cout << "|                 Anda tidak dapat login sebagai admin                 |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
		
	} else if (a==90){
		gotoxy (23,15); cout << "|                         Anda berhasil login                          |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
		
	} else if (a==99){
		gotoxy (23,15); cout << "|    Terima kasih telah menggunakan aplikasi Minimarket K-BANG MART    |" << endl;
		gotoxy (23,16); cout << "|                                                                      |" << endl;
		
	}
	 
	gotoxy (23,17); cout << "+"<< string(70, '=') << "+" << endl;
}





int RiwayatOrAntrianPembeli(pembeli* HEAD){
	system("cls");
 	system("color 17");
 	
 	int x_pos = 8, y_pos = 7;
 	string judul = ( HEAD == TAIL_RiwayatPembeli )? "Riwayat" : ( HEAD == HEAD_Antrian ) ? "Antrian" : "";
 	
 	gotoxy(x_pos, 2); cout << "+" << string(103, '=') << "+" << endl;
	gotoxy(x_pos, 3); cout << "|" << string(32, '-') <<	judul << " Pembeli Mini Market K-BANG MART"	<< string(32, '-') << "|" << endl;
	gotoxy(x_pos, 4); cout << "+=============+================================+==============================+=======+=================+" << endl;
	gotoxy(x_pos, 5); cout << "| No. Antrian |          Nama pembeli          |      Barang yang dibeli      |  Qty. |      Harga      |" << endl;
	gotoxy(x_pos, 6); cout << "+=============+================================+==============================+=======+=================+" << endl;
	pembeli* ptr = HEAD;
	
	if( HEAD == NULL){
		gotoxy(x_pos, 7);cout << "|                                              DATA KOSONG                                              |" << endl;
	} else {
		while ( ptr != NULL) {
			gotoxy(x_pos, y_pos++); cout << "|" << setw(12)<< ptr->noAntrian << " | " << setw(30)	<< ptr->nama <<" |" 
			<< setw(31) <<"|" << setw(8) << "|"  << setw(18) << " |" << endl;
			
			keranjang* ptrKeranjang = ptr->HeadKrj;
			while ( ptrKeranjang  != NULL) {
				gotoxy(x_pos, y_pos++); 
				cout << "|" << setw(14) << "|" << setw(33) << "|" 
					<< setw(29) << ptrKeranjang->nama << " |"
					<< setw(6) << ptrKeranjang->jumlah << " | Rp. "  << setw(11) << ptrKeranjang->harga << " |" << endl;	
				ptrKeranjang = ptrKeranjang->next;
			}
			
			gotoxy(x_pos, y_pos++); cout << "|             +--------------------------------+------------------------------+-------+-----------------+" << endl;
			gotoxy(x_pos, y_pos++); cout << "|" << setw(33) << 	"| Pembelian tanggal " << setw(3) << ptr->hari << setw(3) << "-" 
				<< setw(3) << ptr->bulan << setw(3)<< "-" << setw(5) << ptr->tahun
				<< setw(6) << " Jam " << setw(3) << ptr->jam << setw(3) << ":" << setw(3) << ptr->menit  
				<< setw(25) << "| TOTAL | Rp." << setw(12) << ptr->harga << " |"<<endl;
			gotoxy(x_pos, y_pos++); cout << "+-------------+--------------------------------+------------------------------+-------+-----------------+" << endl;
			
			ptr = ( HEAD == TAIL_RiwayatPembeli )? ptr->prev : ( HEAD == HEAD_Antrian ) ? ptr->next : ptr;
			
		}
	}
	
	gotoxy(x_pos, y_pos++); cout << "|"		<< string(38, '-') 	<<	" Tekan untuk melanjutkan.. "	<<  string(38, '-')  << 	  "|" << endl;
	gotoxy(x_pos, y_pos++); cout << "+" << string(103, '=') << "+" << endl;
	getch();
	return y_pos;
}

void ManajemenAntrianPembeli(){
	int x_pos = 8, y_pos = RiwayatOrAntrianPembeli(HEAD_Antrian);
	y_pos++;
	if( HEAD_Antrian == NULL ){
		gotoxy(x_pos, y_pos++); cout << "+" << string(103, '=') << "+" << endl;
		gotoxy(x_pos, y_pos++); cout << "|"		<< string(43, '-') 	<<	" Antrian Kosong! "	<<  string(43, '-')  << 	  "|" << endl;
		gotoxy(x_pos, y_pos++); cout << "|"		<< string(34, '-') 	<<	" Tekan untuk kembali ke Menu Admin "	<<  string(34, '-')  << 	  "|" << endl;
		gotoxy(x_pos, y_pos++); cout << "+" << string(103, '=') << "+" << endl;
		getch();
		
	} else {
		string input;
		pembeli* AntrianPertama = getFirstAntrian();
		
		gotoxy(x_pos, y_pos++); cout << "+" << string(103, '=') << "+" << endl;
		gotoxy(x_pos, y_pos++); cout << "| 1. Tandai Sebagai Selesai                                                                             |" << endl;
		if( AntrianPertama->metode_pembayaran == "Cash" ){
			gotoxy(x_pos, y_pos++); cout << "| 2. Hapus dari Antrian                                                                                 |" << endl;
		}
		gotoxy(x_pos, y_pos++); cout << "+" << string(103, '-') << "+" << endl;
		gotoxy(x_pos, y_pos++); cout << "| 0. Kembali ke Menu Admin                                                                              |" << endl;
		gotoxy(x_pos, y_pos++); cout << "+" << string(103, '=') << "+" << endl;
		if (input == "1"){
			enqueueRiwayatOrAntrian( AntrianPertama, &HEAD_RiwayatPembeli, &TAIL_RiwayatPembeli);
			dequeueRiwayatOrAntrian(&HEAD_Antrian, &TAIL_Antrian);
			ManajemenAntrianPembeli();
		} else if (input == "2" && AntrianPertama->metode_pembayaran == "Cash"){
			dequeueRiwayatOrAntrian(&HEAD_Antrian, &TAIL_Antrian);
			ManajemenAntrianPembeli();
		} else if (input != "0"){
			Message(1);
			Sleep(2);
			ManajemenAntrianPembeli();
		}
		
	}
	
	
}

int DaftarProduk(string kategori){
	system("cls");
	system("color 17");
	
	int y_pos = 8, i;
	produk* ptr;
	
	gotoxy(23, 2); cout << "+======================================================================+" << endl;
	gotoxy(23, 3); cout << "|" << string(17, '-') <<	"Daftar Produk Minimarket K-BANG MART"	 << string(17, '-') << "|" << endl;
	gotoxy(23, 4); cout << "+======+====================================+======+===================+" << endl;
	gotoxy(23, 5); cout << "| No.  |             Nama produk            | Stok |       Harga       |" << endl;
	gotoxy(23, 6); cout << "+======+====================================+======+===================+" << endl;
	gotoxy(23, 7); cout << "|"					<< 		string(70, ' ') 				<<		   "|" << endl;
	
	if (kategori == "makanan" || kategori == "admin"){
		i = 1;
		ptr = HEAD_Makanan;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		gotoxy(23, y_pos++); cout << "| Makanan                                                              |" << endl;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		while( ptr != NULL ){
			gotoxy(23, y_pos++); cout << "|" << setw(4)<< i << ". | " << setw(34)<< ptr->nama <<" |" 
				<< setw(5) << ptr->stok  <<" | Rp. "  << setw(13) << ptr->harga << " |" << endl;	
			ptr = ptr->next;
			i++;
		}
	}
	
	if (kategori == "minuman" || kategori == "admin"){
		i = 1;
		ptr = HEAD_Minuman;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		gotoxy(23, y_pos++); cout << "| Minuman                                                              |" << endl;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		while( ptr != NULL ){
			gotoxy(23, y_pos++); cout << "|" << setw(4)<< i << ". | " << setw(34)<< ptr->nama <<" |" 
				<< setw(5) << ptr->stok  <<" | Rp. "  << setw(13) << ptr->harga << " |" << endl;	
			ptr = ptr->next;
			i++;
		}
	}
	
	if (kategori == "obat" || kategori == "admin"){
		i = 1;
		ptr = HEAD_Obat;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		gotoxy(23, y_pos++); cout << "| Obat-obatan                                                          |" << endl;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		while( ptr != NULL ){
			gotoxy(23, y_pos++); cout << "|" << setw(4)<< i << ". | " << setw(34)<< ptr->nama <<" |" 
				<< setw(5) << ptr->stok  <<" | Rp. "  << setw(13) << ptr->harga << " |" << endl;	
			ptr = ptr->next;
			i++;
		}
	}
	
	if (kategori == "atk" || kategori == "admin"){
		i = 1;
		ptr = HEAD_ATK;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		gotoxy(23, y_pos++); cout << "| Alat Tulis Kerja                                                     |" << endl;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		while( ptr != NULL ){
			gotoxy(23, y_pos++); cout << "|" << setw(4)<< i << ". | " << setw(34)<< ptr->nama <<" |" 
				<< setw(5) << ptr->stok  <<" | Rp. "  << setw(13) << ptr->harga << " |" << endl;	
			ptr = ptr->next;
			i++;
		}
	}
	
	if (kategori == "perabot" || kategori == "admin"){
		i = 1;
		ptr = HEAD_Perabot;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		gotoxy(23, y_pos++); cout << "| Perabotan Rumah                                                      |" << endl;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		while( ptr != NULL ){
			gotoxy(23, y_pos++); cout << "|" << setw(4)<< i << ". | " << setw(34)<< ptr->nama <<" |" 
				<< setw(5) << ptr->stok  <<" | Rp. "  << setw(13) << ptr->harga << " |" << endl;	
			ptr = ptr->next;
			i++;
		}
	}
	
	if (kategori == "elektronik" || kategori == "admin"){
		i = 1;
		ptr = HEAD_Elektronik;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		gotoxy(23, y_pos++); cout << "| Alat / Perkakas Elektronik                                           |" << endl;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		while( ptr != NULL ){
			gotoxy(23, y_pos++); cout << "|" << setw(4)<< i << ". | " << setw(34)<< ptr->nama <<" |" 
				<< setw(5) << ptr->stok  <<" | Rp. "  << setw(13) << ptr->harga << " |" << endl;	
			ptr = ptr->next;
			i++;
		}
	}
	
	if (kategori == "peralatanbayi" || kategori == "admin"){
		i = 1;
		ptr = HEAD_PeralatanBayi;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		gotoxy(23, y_pos++); cout << "| Peralatan Bayi                                                       |" << endl;
		gotoxy(23, y_pos++); cout << "+------+------------------------------------+------+-------------------+" << endl;
		while( ptr != NULL ){
			gotoxy(23, y_pos++); cout << "|" << setw(4)<< i << ". | " << setw(34)<< ptr->nama <<" |" 
				<< setw(5) << ptr->stok  <<" | Rp. "  << setw(13) << ptr->harga << " |" << endl;	
			ptr = ptr->next;
			i++;
		}
	}  
	
	
	gotoxy(23, y_pos++); cout << "+======+====================================+======+===================+" << endl << endl;
	gotoxy(23, y_pos++); cout << "|"		<< string(23, '-') 	<<	" Tekan untuk melanjutkan "	<<  string(22, '-')  << 	  "|" << endl;
	gotoxy(23, y_pos++); cout << "+" 				<< 	string(70, '=') 		<< "+" << endl;
	getch();
	return y_pos;
}

produk* RincianProduk(string kategori,int i){
	system("cls");
	produk* rincian = getProduk(i,kategori);
	gotoxy(23, 2); cout << "+===================================+==================================+" << endl;
	gotoxy(23, 3); cout << "| " << setw(33)  << rincian->nama <<" |"<< setw(35) << "|" << endl;
	gotoxy(23, 4); cout << "+-----------------------------------+----------------------------------+" << endl;
	gotoxy(23, 5); cout << "| Harga: Rp. " << setw(22) << rincian->harga 	<< " | Stok: "  <<	setw(26)  << rincian->stok <<" |" << endl;
	gotoxy(23, 6); cout << "+===================================+==================================+" << endl;
	
	return rincian;
}


int KategoriProduk(){
	int input, x_pos = 28;
	string input_kategori;
	system("cls");
	hiasan1();
	gotoxy(x_pos, 2);  cout << "+" << string(70, '=') << "+" << endl;
	gotoxy(x_pos, 3);  cout << "|                          KATEGORI PRODUK                             |" << endl;
	gotoxy(x_pos, 4);  cout << "+" << string(70, '=') << "+" << endl;
	gotoxy(x_pos, 5);  cout << "| 1. Makanan                                                           |" << endl;
	gotoxy(x_pos, 6);  cout << "| 2. Minuman                                                           |" << endl;
	gotoxy(x_pos, 7);  cout << "| 3. Obat-obatan                                                       |" << endl;
	gotoxy(x_pos, 8);  cout << "| 4. Alat Tulis Kerja                                                  |" << endl;
	gotoxy(x_pos, 9);  cout << "| 5. Perabotan Rumah                                                   |" << endl;
	gotoxy(x_pos, 10); cout << "| 6. Alat / Perkakas Elektronik                                        |" << endl;
	gotoxy(x_pos, 11); cout << "| 7. Peralatan Bayi                                                    |" << endl;
	gotoxy(x_pos, 12); cout << "+----------------------------------------------------------------------+" << endl;
	gotoxy(x_pos, 13); cout << "| 8. Kembali ke menu                                                   |" << endl;
	gotoxy(x_pos, 14); cout << "+" << string(70, '=') << "+" << endl;
	gotoxy(x_pos, 15); cout << " Masukan pilihan anda : "; 
	cin >> input_kategori;
	fflush(stdin);
	input = atoi(input_kategori.c_str()); 
	if ( !(input > 0 && input < 9) ){
		Message(1);
		sleep(2);
	}
	return input;
}


void TambahProduk(){
	int stok_int, harga_int, pilih = KategoriProduk(), x_pos = 35;
	string nama, stok_string, harga_string;
	
	system("cls");
	hiasan1();
	if ( pilih >  0 && pilih < 6){
		gotoxy(x_pos, 10); cout << "+----------------------------------------------------+" << endl;
		gotoxy(x_pos, 11);	cout << "|                   Tambah Produk                    |";
		gotoxy(x_pos, 12); cout << "+----------------------------------------------------+" << endl;
		gotoxy(x_pos, 13); cout << "  Nama produk       : "; 
		getline (cin, nama);
		
		gotoxy(x_pos, 14); cout << "  Harga produk      : Rp. "; 
		getline (cin, harga_string);
		
		gotoxy(x_pos, 15); cout << "  Jumlah stok       : "; 
		getline (cin, stok_string);
		
		stok_int  = atoi ( stok_string.c_str());
		harga_int = atoi ( harga_string.c_str());

		if ( harga_int < 1 ){
			Message(11);
			sleep(2);
			
		} else if ( stok_int < 1 || stok_int > 9999 ){
			Message(12);
			sleep(2);
			
		} else {
			Loading("tambah");
			produk* produkBaru = new produk();
			produkBaru->nama = nama;
			produkBaru->stok = stok_int;
			produkBaru->harga = harga_int;
			produkBaru->next = NULL;
			produkBaru->prev = NULL;
			if ( pilih == 1){
				insertDoubleLast(produkBaru, &HEAD_Makanan, &TAIL_Makanan);
				sortNama_Makanan = false;
			} else if ( pilih == 2){
				insertDoubleLast(produkBaru, &HEAD_Minuman, &TAIL_Minuman);
				sortNama_Minuman = false;
			} else if ( pilih == 3){
				insertDoubleLast(produkBaru, &HEAD_Obat, &TAIL_Obat);
				sortNama_Obat = false;
			} else if ( pilih == 4){
				insertDoubleLast(produkBaru, &HEAD_ATK, &TAIL_ATK);
				sortNama_ATK = false;
			}else if ( pilih == 5){
				insertDoubleLast(produkBaru, &HEAD_Perabot, &TAIL_Perabot);
				sortNama_Perabot = false;
			} else if ( pilih == 6){
				insertDoubleLast(produkBaru, &HEAD_Elektronik, &TAIL_Elektronik);
				sortNama_Elektronik = false;
			} else if ( pilih == 7){
				insertDoubleLast(produkBaru, &HEAD_PeralatanBayi, &TAIL_PeralatanBayi);
				sortNama_PltBayi = false;
			}
			Message(10);
			sleep(3);
		}
	}
}


void sortirProduk(produk** HEAD, produk** TAIL, produk data[], int jumlah, string jenis, int urutan, bool* sortStatus){	
	//Urutan 1: asc, urutan 2: desc
	if((*HEAD) != NULL){
		
		assignLinkedListToArray(*HEAD, *TAIL, data);
		
		if( (*sortStatus) == 0){
			quickSort(data, jenis, 0, jumlah - 1, urutan);
			assignArrayToLinkedList(HEAD, TAIL, data);
			(*sortStatus) = true;
		}
	}
}


void UbahProduk(int index_search, string kategori_cari){
	system("cls");	
	
	if (kategori_cari == "makanan" 
	 || kategori_cari == "minuman" 
	 || kategori_cari == "obat" 
	 || kategori_cari == "atk" 
	 || kategori_cari == "perabot"
	 || kategori_cari == "elektronik" 
	 || kategori_cari == "peralatanbayi"){
	 	hiasan();
		string input_ubah, ubah;
		RincianProduk(kategori_cari,++index_search);
		gotoxy(23, 7);  cout << "+======================================================================+" << endl;
		gotoxy(23, 8);  cout << "| 1. Ubah Nama Produk                                                  |" << endl;
		gotoxy(23, 9);  cout << "| 2. Ubah Harga Produk                                                 |" << endl;
		gotoxy(23, 10); cout << "| 3. Ubah Stok Produk                                                  |" << endl;
		gotoxy(23, 11); cout << "+----------------------------------------------------------------------+" << endl;
		gotoxy(23, 12); cout << "| 0. Kembali ke menu                                                   |" << endl;
		gotoxy(23, 13); cout << "+======================================================================+" << endl;
		gotoxy(23, 14); cout << " Masukan pilihan anda : "; 
		
		getline(cin,input_ubah);
		fflush(stdin);
		
		bool ubah_berhasil;
		
		if ( input_ubah == "1" ){
			gotoxy(23, 15); cout << " Nama produk setelah perubahan : "; 
			getline(cin,ubah);
			fflush(stdin);
			
			if (kategori_cari == "makanan"){
				getProduk(index_search, kategori_cari)->nama = ubah;
				sortNama_Makanan = false;
				sortNama_Minuman = false;
				sortNama_Obat = false;
				sortNama_ATK = false;
				sortNama_Perabot = false;
				sortNama_Elektronik = false;
				sortNama_PltBayi = false;
				
			}
			ubah_berhasil = true;
		}  else if ( input_ubah == "2"){
			int ubah_harga;
			gotoxy(23, 15); cout << " Harga produk setelah perubahan : "; 
			getline(cin,ubah);
			fflush(stdin);
			
			ubah_harga  = atoi ( ubah.c_str());
			if ( ubah_harga < 1 ) {
				Message(11);
				sleep(2);
			} else {
				getProduk(index_search, kategori_cari)->harga = ubah_harga;
				ubah_berhasil = true;
			}
			 
			
		} else if ( input_ubah == "3"){
			int ubah_stok;
			gotoxy(23, 15); cout << " Stok produk setelah perubahan : "; 
			getline(cin,ubah);
			fflush(stdin);
			
			ubah_stok  = atoi ( ubah.c_str());
			if ( ubah_stok < 1 || ubah_stok > 9999) {
				Message(12);
				sleep(2);
			} else {
				getProduk(index_search, kategori_cari)->stok = ubah_stok;
				ubah_berhasil = true;
			}
		}  else if ( input_ubah != "0" ) {
			Message(1);
			sleep(2);
		}
			
		if ( ubah_berhasil == true ){
			Loading("ubah");
			Message(20);
			sleep(2);
		}
			
	} else {
		Message(77);
		sleep(2);
	}
}

void HapusProduk(int index_search, string kategori_cari){
	system("cls");
	
	if (kategori_cari == "makanan" 
	 || kategori_cari == "minuman" 
	 || kategori_cari == "obat" 
	 || kategori_cari == "atk" 
	 || kategori_cari == "perabot"
	 || kategori_cari == "elektronik" 
	 || kategori_cari == "peralatanbayi" ){
	 	hiasan();
		string input, konfir;
		RincianProduk(kategori_cari,++index_search);
		
		gotoxy(23, 7); cout << "+======================================================================+" << endl;
		gotoxy(23, 8); cout << "| 1. Hapus produk                                                      |" << endl;
		gotoxy(23, 9); cout << "+----------------------------------------------------------------------+" << endl;
		gotoxy(23, 10); cout << "| 0. Kembali ke menu                                                   |" << endl;
		gotoxy(23, 11); cout << "+======================================================================+" << endl;
		gotoxy(23, 12); cout << " Masukan pilihan anda : "; 
		getline (cin, input );
		fflush(stdin);
		
		if ( input == "1" ) {
			gotoxy(23, 13); cout << "  PENGHAPUSAN INI TIDAK DAPAT DIBATALKAN" << endl;
			gotoxy(23, 14); cout << "  Anda yakin ingin menghapus produk ini? (y/t): ";
			
			getline (cin,konfir); fflush(stdin);
			if (konfir == "y" || konfir == "Y" ){
				if (kategori_cari == "makanan"){
					if(index_search == 1){
						deleteDoubleFirst(&HEAD_Makanan, &TAIL_Makanan);
					}else if(index_search == length(HEAD_Makanan)){
						deleteDoubleLast(&HEAD_Makanan, &TAIL_Makanan);
					}else{
						deleteDoubleAt(index_search, &HEAD_Makanan);
					}
				} else if (kategori_cari == "minuman"){
					if(index_search == 1){
						deleteDoubleFirst(&HEAD_Minuman, &TAIL_Minuman);
					}else if(index_search == length(HEAD_Minuman)){
						deleteDoubleLast(&HEAD_Minuman, &TAIL_Minuman);
					}else{
						deleteDoubleAt(index_search, &HEAD_Minuman);
					}
				} else if (kategori_cari == "obat"){
					if(index_search == 1){
						deleteDoubleFirst(&HEAD_Obat, &TAIL_Obat);
					}else if(index_search == length(HEAD_Obat)){
						deleteDoubleLast(&HEAD_Obat, &TAIL_Obat);
					}else{
						deleteDoubleAt(index_search, &HEAD_Obat);
					}
				} else if (kategori_cari == "atk"){
					if(index_search == 1){
						deleteDoubleFirst(&HEAD_ATK, &TAIL_ATK);
					}else if(index_search == length(HEAD_ATK)){
						deleteDoubleLast(&HEAD_ATK, &TAIL_ATK);
					}else{
						deleteDoubleAt(index_search, &HEAD_ATK);
					}
				} else if (kategori_cari == "perabot"){
					if(index_search == 1){
						deleteDoubleFirst(&HEAD_Perabot, &TAIL_Perabot);
					}else if(index_search == length(HEAD_Perabot)){
						deleteDoubleLast(&HEAD_Perabot, &TAIL_Perabot);
					}else{
						deleteDoubleAt(index_search, &HEAD_Perabot);
					}
				} else if (kategori_cari == "elektronik"){
					if(index_search == 1){
						deleteDoubleFirst(&HEAD_Elektronik, &TAIL_Elektronik);
					}else if(index_search == length(HEAD_Elektronik)){
						deleteDoubleLast(&HEAD_Elektronik, &TAIL_Elektronik);
					}else{
						deleteDoubleAt(index_search, &HEAD_Elektronik);
					}
				} else if (kategori_cari == "peralatanbayi"){
					if(index_search == 1){
						deleteDoubleFirst(&HEAD_PeralatanBayi, &TAIL_PeralatanBayi);
					}else if(index_search == length(HEAD_PeralatanBayi)){
						deleteDoubleLast(&HEAD_PeralatanBayi, &TAIL_PeralatanBayi);
					}else{
						deleteDoubleAt(index_search, &HEAD_PeralatanBayi);
					}
				}
				Loading("hapus");
				Message(30);
				sleep(2);
				
			}
		} else if ( input != "0" ) {
			Message(1);
			sleep(2);
			
		}
	} else {
		Message(77);
		sleep(2);
	}
	
}

void UbahHapusSearch(string action){
	system("cls");
	hiasan1();

	string input_cari,kategori_cari;
	
	int jml_makanan = length(HEAD_Makanan),
		jml_minuman = length(HEAD_Minuman),
		jml_obat 	= length(HEAD_Obat),
		jml_atk 	= length(HEAD_ATK),
		jml_perabot = length(HEAD_Perabot),
		jml_elektronik = length(HEAD_Elektronik),
		jml_pltbayi = length(HEAD_PeralatanBayi);
	
	produk  tmpMakanan[ jml_makanan ], tmpMinuman[ jml_minuman ], tmpObat[ jml_obat ], tmpATK[ jml_atk ],
			tmpPerabot[ jml_perabot ], tmpElektronik[ jml_elektronik ], tmpPltBayi[ jml_pltbayi ];
	
	sortirProduk( &HEAD_Makanan, 		&TAIL_Makanan, 		tmpMakanan, 	jml_makanan, 	"nama", 1, &sortNama_Makanan);
	sortirProduk( &HEAD_Minuman, 		&TAIL_Minuman,		tmpMinuman, 	jml_minuman,	"nama", 1, &sortNama_Minuman);
	sortirProduk( &HEAD_Obat, 			&TAIL_Obat,			tmpObat, 		jml_obat,		"nama", 1, &sortNama_Obat);
	sortirProduk( &HEAD_ATK, 			&TAIL_ATK,			tmpATK, 		jml_atk,		"nama", 1, &sortNama_ATK);
	sortirProduk( &HEAD_Perabot, 		&TAIL_Perabot, 		tmpPerabot, 	jml_perabot,	"nama", 1, &sortNama_Perabot);
	sortirProduk( &HEAD_Elektronik, 	&TAIL_Elektronik, 	tmpElektronik, 	jml_elektronik,	"nama", 1, &sortNama_Elektronik);
	sortirProduk( &HEAD_PeralatanBayi, &TAIL_PeralatanBayi,	tmpPltBayi, 	jml_pltbayi, 	"nama", 1, &sortNama_PltBayi);
	
	int tmp_indeks, indeks, y_pos = DaftarProduk("admin"),i=0;
	gotoxy(23, y_pos++); cout << " Masukan nama produk : "; 
	getline(cin,input_cari);
	
	
	if( jml_makanan != 0){
		tmp_indeks = fibonacciSearch(tmpMakanan, 	input_cari, jml_makanan );
		if (tmp_indeks != -1){ indeks = tmp_indeks; kategori_cari = "makanan"; }
	}
	if( jml_minuman != 0){
		tmp_indeks = fibonacciSearch(tmpMinuman,	input_cari, jml_minuman );
		if (tmp_indeks != -1){ indeks = tmp_indeks; kategori_cari = "minuman"; }
	}
	if( jml_obat != 0){
		tmp_indeks = fibonacciSearch(tmpObat, 		input_cari, jml_obat );
		if (tmp_indeks != -1){ indeks = tmp_indeks; kategori_cari = "obat"; }
	}
	if( jml_atk != 0){
		tmp_indeks = fibonacciSearch(tmpATK, 		input_cari, jml_atk );
		if (tmp_indeks != -1){ indeks = tmp_indeks; kategori_cari = "atk"; }
	}
	if( jml_perabot != 0){
		tmp_indeks = fibonacciSearch(tmpPerabot, 	input_cari, jml_perabot );
		if (tmp_indeks != -1){ indeks = tmp_indeks; kategori_cari = "perabot"; }	
	}
	if( jml_elektronik != 0){
		tmp_indeks = fibonacciSearch(tmpElektronik,	input_cari, jml_elektronik );
		if (tmp_indeks != -1){ indeks = tmp_indeks; kategori_cari = "elektronik"; }
	}
	
		
	
	if( jml_pltbayi != 0){
		tmp_indeks = fibonacciSearch(tmpPltBayi, input_cari, jml_pltbayi );
		if (tmp_indeks != -1){ indeks = tmp_indeks; kategori_cari = "peralatanbayi"; }
	}
	
	Loading("cari");
	if ( action == "ubah"){
		UbahProduk(indeks,kategori_cari);
	} else if (action == "hapus"){
		HapusProduk(indeks,kategori_cari);
	}
	

}

void DataProduk(){
	char keluar='n';
	while(keluar=='n'){
		string input; int x_pos = 28;
		
		system("cls");
		hiasan1();
		gotoxy(x_pos, 2); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(x_pos, 3); cout << "|                              DATA PRODUK                             |" << endl;
		gotoxy(x_pos, 4); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(x_pos, 5); cout << "| 1. Tambah produk                                                     |" << endl;
		gotoxy(x_pos, 6); cout << "| 2. Daftar produk                                                     |" << endl;
		gotoxy(x_pos, 7); cout << "| 3. Edit produk                                                       |" << endl;
		gotoxy(x_pos, 8); cout << "| 4. Hapus produk                                                      |" << endl;
		gotoxy(x_pos, 9); cout << "+----------------------------------------------------------------------+" << endl;
		gotoxy(x_pos, 10); cout << "| 0. Kembali ke menu                                                   |" << endl;
		gotoxy(x_pos, 11); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(x_pos, 12); cout << " Masukan pilihan anda : "; 
		cin >> input;
		fflush(stdin);
		if (input == "1"){
			string nama;
			system("cls");
			TambahProduk();
			
		} else if (input == "2"){
			DaftarProduk("admin");
			
		} else if (input == "3"){
			UbahHapusSearch("ubah");
			
		} else if (input == "4"){
			UbahHapusSearch("hapus");
			
		} else if (input == "0"){
			keluar = 'y';
			
		} else {
			Message(1);
			sleep(2);
		}	
	}
}


string GenerateTanggal(){
	time_t sekarang = time(0);
	struct tm *jam = localtime(&sekarang);
	
	int hari = jam-> tm_mday;
	int bulan = 1 + jam-> tm_mon;
	int tahun = 1900 + jam-> tm_year;
	
	tahun %= 100;
	
	string hasil;
	stringstream ss_data;
	if ( tahun < 10){
		ss_data<< "0";
	}
	ss_data<< tahun;
	if ( bulan < 10){
		ss_data<< "0";
	}
	ss_data<< bulan;
	if ( hari < 10){
		ss_data<< "0";
	}
	ss_data<< hari;
	
	ss_data>> hasil;
	return hasil;
}

void MasukAntrian(pembeli* pembeliBaru, string metode_pembayaran){
	time_t sekarang = time(0);
	struct tm *jam = localtime(&sekarang);
	
	int jam_now 	= jam->tm_hour ;
	int menit 		= jam->tm_min ;
	
	int hari = jam-> tm_mday;
	int bulan = 1 + jam-> tm_mon;
	int tahun = 1900 + jam-> tm_year;
	
	
	string noAntrian = GenerateTanggal();
	
	string CURR_Antrian_str;
	stringstream ss_data;
	ss_data<< CURR_Antrian;
	ss_data>> CURR_Antrian_str;
	
	noAntrian = noAntrian + "-" + CURR_Antrian_str;
	pembeliBaru->noAntrian = noAntrian;
						
	pembeliBaru->hari  = hari;
	pembeliBaru->bulan = bulan;
	pembeliBaru->tahun = tahun;
	pembeliBaru->jam   = jam_now;
	pembeliBaru->menit = menit;
	
	system("cls");
	gotoxy (23,13); cout << "+"<< string(70, '=') << "+" << endl;
	gotoxy (23,14); cout << "|                                                                      |" << endl;	
	gotoxy (23,15); cout << "|                     Nomor Antrian Anda: " << setw(14) << noAntrian << setw(16) << "|" << endl;
	if ( metode_pembayaran == "Cash" ){
		pembeliBaru->metode_pembayaran = metode_pembayaran;
		gotoxy (23,16); cout << "|              Silahkan mengantri ke kasir untuk membayar              |" << endl;
	} else if ( metode_pembayaran == "Kartu ATM" ){
		pembeliBaru->metode_pembayaran = metode_pembayaran;
		gotoxy (23,16); cout << "|       Silahkan mengantri ke kasir untuk mengambil barang anda.       |" << endl;
	}
	gotoxy (23,17); cout << "|                                                                      |" << endl;
	gotoxy (23,18); cout << "+"<< string(70, '=') << "+" << endl;
	enqueueRiwayatOrAntrian(pembeliBaru, &HEAD_Antrian, &TAIL_Antrian);
	CURR_Antrian++;
	getch();
}

void Transaksi(){
	hiasan();
	system("cls");
	int x_pos = 7;
	gotoxy (x_pos,1);  cout << "++" << string(100, '=') << "++" << endl;
	gotoxy (x_pos,2);  cout << "|                                                                                                    |" << endl;
	gotoxy (x_pos,3);  cout << "|                                 Mohon siapkan alat pembayaran anda.                                |" << endl;
	gotoxy (x_pos,4);  cout << "|                                                                                                    |" << endl;
	gotoxy (x_pos,5);  cout << "|                                       /+------------------------+                                  |" << endl;
	gotoxy (x_pos,6);  cout << "|                                      / |                        |                                  |" << endl;
	gotoxy (x_pos,7);  cout << "|                                     /  |      atm.txt           |                                  |" << endl;
	gotoxy (x_pos,8);  cout << "|                                    /   |                        |                                  |" << endl;
	gotoxy (x_pos,9);  cout << "|                                   /    |     Isi File:          |                                  |" << endl;
	gotoxy (x_pos,10); cout << "|                                  /_____|                        |                                  |" << endl;
	gotoxy (x_pos,11); cout << "|                                  |                              |                                  |" << endl;
	gotoxy (x_pos,12); cout << "|                                  |  nama                        |                                  |" << endl;
	gotoxy (x_pos,13); cout << "|                                  |  no. rekening                |                                  |" << endl;
	gotoxy (x_pos,14); cout << "|                                  |  saldo                       |                                  |" << endl;
	gotoxy (x_pos,15); cout << "|                                  |                              |                                  |" << endl;
	gotoxy (x_pos,16); cout << "|                                  | Contoh file pembelian pulsa: |                                  |" << endl;
	gotoxy (x_pos,17); cout << "|                                  |                              |                                  |" << endl;
	gotoxy (x_pos,18); cout << "|                                  |  Dede Rizki                  |                                  |" << endl;
	gotoxy (x_pos,19); cout << "|                                  |  001100220033                |                                  |" << endl;
	gotoxy (x_pos,20); cout << "|                                  |  420420                      |                                  |" << endl;
	gotoxy (x_pos,21); cout << "|                                  |                              |                                  |" << endl;
	gotoxy (x_pos,22); cout << "|                                  +------------------------------+                                  |" << endl;
	gotoxy (x_pos,23); cout << "|                                                                                                    |" << endl;
	gotoxy (x_pos,24); cout << "|                                                                                                    |" << endl;
	gotoxy (x_pos,25); cout << "++" << string(100, '=') << "++" << endl;
	gotoxy (x_pos,26); cout << "|                                        Tekan untuk membayar                                        |" << endl;	
	gotoxy (x_pos,27); cout << "++" << string(100, '=') << "++" << endl;	
	getch();
}

void Pembayaran(pembeli* pembeliBaru, string nama, string metode_pembayaran, string no_id){
	//Deteksi waktu
	
	if ( metode_pembayaran == "Cash" ){
		MasukAntrian(pembeliBaru, metode_pembayaran);
	} else if ( metode_pembayaran == "Kartu ATM" ){
		int replace = 0;
		
		Transaksi();

		ofstream temp; 
		temp.open("temp.txt", ios::out);
		fstream TheFile; 
		TheFile.open("atm.txt", ios::in);
		
	    if (TheFile.is_open()) {
	        string Line;
	
	        int line_no = 1, saldo=0;
	
	        while ( getline(TheFile, Line) ) {
	        	if (line_no == 1 && Line == nama ) {
				} else if (line_no == 2 && Line == no_id){
			  	} else if (line_no == 3){
	  				saldo = atoi (Line.c_str()); 
	  				if ( saldo >= pembeliBaru->harga){
	  					
	  					saldo = saldo -  pembeliBaru->harga;
	  					temp << nama << endl;
	  					temp << no_id << endl;
	  					temp << saldo << endl;
	  					
	  					replace = 1;
	  					
	  					system("cls");
						gotoxy (23,15); cout << "+"<< string(70, '=') << "+" << endl;
						gotoxy (23,16); cout << "|                       Pembelian Produk Berhasil                      |" << endl;
						gotoxy (23,17); cout << "|                     Sisa Saldo: Rp. "<< setw(14) << saldo << setw(20) << "|" << endl;
						gotoxy (23,18); cout << "+"<< string(70, '=') << "+" << endl;
						sleep(2);
						
	  					
	  					
			   		 	MasukAntrian(pembeliBaru, metode_pembayaran);
	  					TheFile.close(); 
	  					
					  } else {
					  	temp << nama << endl;
	  					temp << no_id << endl;
	  					temp << saldo << endl;
						replace = 1;
						pembeliBaru = NULL;
					  	Message(5);
			  			sleep(2);
					  }
					break;
					
			  	}	else {
			  		Message(4);
			  		sleep(2);
			  		pembeliBaru = NULL;
					break;
				}
				line_no++;
			}
				
			
	    } else {
	        Message(6);
	        sleep(2);
	
	    }
	
	    TheFile.close();
	    temp.close();
	    
	    if ( replace == 1){
	    	remove("atm.txt");
			rename("temp.txt","atm.txt");
		}
	}
	
}

void KeranjangBelanja(pembeli* pembeliBaru){
	string opsi, nama, no_id, metode_pembayaran;
	int x_pos = 23, y_pos = 2, i=0, totalHarga = 0;
	
	system("cls");
	gotoxy(x_pos, y_pos++); cout << "+===================================+==================================+" << endl;
	gotoxy(x_pos, y_pos++); cout << "| Keranjang Anda" 				<< 				setw(56) 			<< "|" << endl;
	gotoxy(x_pos, y_pos++); cout << "+-----+--------------------------------------+-------+-----------------+" << endl;
	gotoxy(x_pos, y_pos++); cout << "| No. |          Barang yang dibeli          |  Qty. |      Harga      |" << endl;
	gotoxy(x_pos, y_pos++); cout << "+-----+--------------------------------------+-------+-----------------+" << endl;
	
	keranjang* ptr = pembeliBaru->HeadKrj;
	while(ptr != NULL){
		i++;
		gotoxy(x_pos, y_pos++); cout << "| " << setw(3) << i << " |" << setw(37) << ptr->nama << " |"
					<< setw(6) << ptr->jumlah 	<< " | Rp."  <<	setw(12)  << ptr->harga <<" |" << endl;
		gotoxy(x_pos, y_pos++); cout << "+-----+--------------------------------------+-------+-----------------+" << endl;

		totalHarga += ptr->harga;
		ptr=ptr->next;
	}
	gotoxy(x_pos, y_pos++); cout << "+===================================+==================================+" << endl;
	
	
	
	gotoxy(x_pos, y_pos++); cout << "+" << string(70, '=') << "+" << endl;
	if (pembeliBaru->HeadKrj == NULL){
		gotoxy(x_pos, y_pos++); cout << "|        Tidak ada produk di Keranjang anda. Mari berbelanja :)        |" << endl;	
		gotoxy(x_pos, y_pos++); cout << "|                     Tekan untuk kembali ke menu.                     |" << endl;	
		gotoxy(x_pos, y_pos++); cout << "+" << string(70, '=') << "+" << endl;
		getch();
	} else {
		gotoxy(x_pos, y_pos++); cout << "| 1. Konfirmasi belanja / Checkout                                     |" << endl;	
		gotoxy(x_pos, y_pos++); cout << "| 2. Hapus dari keranjang                                              |" << endl;	
		gotoxy(x_pos, y_pos++); cout << "| 3. Kembali ke menu pembeli                                           |" << endl;	
		gotoxy(x_pos, y_pos++); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(x_pos, y_pos++); cout << "  Pilihan : ";  cin >> opsi;
		fflush(stdin);
		
		if ( opsi == "1"){
			gotoxy(x_pos, y_pos++); cout << " Masukan nama anda       : ";  getline(cin,nama);
			fflush(stdin);
			
			int y_pos_temp = y_pos++;
			
			while(true){
				y_pos = y_pos_temp;
				gotoxy(x_pos, y_pos++); cout << "+" << string(70, '-') << "+" << endl;
				gotoxy(x_pos, y_pos++); cout << "| 1. Cash                                                              |" << endl;	
				gotoxy(x_pos, y_pos++); cout << "| 2. Kartu ATM                                                         |" << endl;	
				gotoxy(x_pos, y_pos++); cout << "+" << string(70, '-') << "+" << endl;
				gotoxy(x_pos, y_pos); cout << "                                                                        ";
				gotoxy(x_pos, y_pos++); cout << " Metode pembayaran (1/2) : ";  cin >> metode_pembayaran;		
				
				metode_pembayaran = (metode_pembayaran == "1") ? "Cash":
									(metode_pembayaran == "2") ? "Kartu ATM": " ";
				if (metode_pembayaran == "Cash" || metode_pembayaran == "Kartu ATM") { break;}
			}
			fflush(stdin);
			if( metode_pembayaran == "Kartu ATM" ){
				gotoxy(x_pos, y_pos++); cout << " No. Rekening anda       : ";  cin >> no_id;
				fflush(stdin);
			}
			
			
			string confirm = "t";
			while (confirm == "t"){
				gotoxy(x_pos, y_pos++); cout << "+" << string(70, '=') << "+" << endl;
				gotoxy(x_pos, y_pos++); cout << "  Konfirmasi" << endl;
				gotoxy(x_pos, y_pos++); cout << "  Nama: " << nama << endl;
				gotoxy(x_pos, y_pos++); cout << "  Metode pembayaran: " << metode_pembayaran << endl;
				if( metode_pembayaran == "Kartu ATM" ){
					gotoxy(x_pos, y_pos++); cout << "  No. Rekening: " << no_id << endl;
				}
				gotoxy(x_pos, y_pos++); cout << "  Konfirmasi pembayaran? (y/t) ";  cin >> confirm;
				if (confirm == "y"){
					confirm = "y";
					
					pembeliBaru->nama  = nama;
					pembeliBaru->jumlah  = i;
					pembeliBaru->harga = totalHarga;
					Pembayaran(pembeliBaru, nama, metode_pembayaran, no_id);
					
				} else if (confirm == "t"){
					confirm = "y";
				} else {
					Message(1);
					sleep(2);
				}
			}
		} else if ( opsi == "2" ){
			int jumlahKrjg = 0;
			string namaProduk, input;
			gotoxy(23, y_pos++ ); cout << " Pilih produk ( tekan 0 untuk kembali ke menu ): "; cin >> input;
		    fflush(stdin);
		    
		    int input_int = atoi(input.c_str());
		    
		    keranjang* ptr = pembeliBaru->HeadKrj; 
		    while (ptr != NULL) { 
		        jumlahKrjg++; 
				if(namaProduk == ptr->nama){
					namaProduk = ptr->nama;
					break;
				}
		        
		        ptr = ptr->next; 
		    }
		    
		    if(input_int != 0 ){
		    	hapusKeranjang(pembeliBaru, namaProduk);
			} else if ( input_int == 0 || input_int < jumlahKrjg ){
				Message(1);
				sleep(2);
			}
			

		} else if ( opsi != "3" ){
			Message(1);
			sleep(2);
		}
	}
	
}

void BeliProduk(string kategori, int index, pembeli* pembeliBaru){
	string opsi, nama, no_id;
	produk* hasil = RincianProduk(kategori,index);	
	
	if ( hasil->stok < 1 ){
		gotoxy(23, 12); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(23, 13); cout << "|                 Maaf produk ini sedang habis stok :(                 |" << endl;	
		gotoxy(23, 14); cout << "|                     Tekan untuk kembali ke menu.                     |" << endl;	
		gotoxy(23, 15); cout << "+" << string(70, '=') << "+" << endl;
		getch();
		
	} else {
		gotoxy(23, 12); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(23, 13); cout << "| 1. Masukkan ke keranjang                                             |" << endl;	
		gotoxy(23, 14); cout << "| 2. Kembali ke menu pembeli                                           |" << endl;	
		gotoxy(23, 15); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(23, 16); cout << "  Pilihan : ";  cin >> opsi;
		fflush(stdin);
		
		if ( opsi == "1"){
			int ada = false;
			keranjang* ptr = pembeliBaru->HeadKrj;
			while(ptr != NULL && ada == false){
				if (ptr->nama == hasil->nama){
					ptr->jumlah ++;
					hasil->stok--;
					ptr->harga += hasil->harga;
					ada = true;
				} else {
					ptr = ptr->next;
				}
			}
			if( ada == false){
				keranjang* baru = new keranjang();
				baru-> jumlah = 0;
				baru->nama = hasil->nama;
				baru-> jumlah++;
				baru->harga = hasil->harga;
				baru->next = NULL;
				 
				hasil->stok--;
				insertKeranjang(baru,pembeliBaru);
			}
			Message(7);
			sleep(2);
			
		} else if ( opsi != "2" ){
			Message(1);
			sleep(2);
		}
	}	 	 
	
}

void PilihProduk(string kategori, int jumlah_produk_pilihan, pembeli* pembeliBaru){
	int y_increment = DaftarProduk(kategori);
	
	if ( jumlah_produk_pilihan < 1 ){
		gotoxy(23, y_increment++ ); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(23, y_increment++ ); cout << "|                         Daftar produk kosong                         |" << endl;	
		gotoxy(23, y_increment++ ); cout << "|                     Tekan untuk kembali ke menu.                     |" << endl;	
		gotoxy(23, y_increment++ ); cout << "+" << string(70, '=') << "+" << endl;
		getch();
		
	} else {
		string input, input2, jenis_sort;
	
		gotoxy(23, y_increment++ ); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(23, y_increment++ ); cout << "| 1. Pilih produk                                                      |" << endl;
		gotoxy(23, y_increment++ ); cout << "| 2. Urutkan harga produk                                              |" << endl;
		gotoxy(23, y_increment++ ); cout << "|----------------------------------------------------------------------|" << endl;	
		gotoxy(23, y_increment++ ); cout << "| 0. Kembali ke menu                                                   |" << endl;
		gotoxy(23, y_increment++ ); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(23, y_increment++ ); cout << " Masukkan pilihan anda: "; cin >> input;
		fflush(stdin);
		
		if ( input == "1"){
			char k='n';
			while (k == 'n'){
			    gotoxy(23, y_increment++ ); cout << " Pilih produk ( tekan 0 untuk kembali ke menu ): "; cin >> input2;
			    fflush(stdin);
			    
			    int input2_int = atoi (input2.c_str());
			    
				if ( input2_int > jumlah_produk_pilihan || input2_int < 0 ) {
					Message(1);
			    	sleep(2);
			    	k = 'y';
			    	PilihProduk(kategori, jumlah_produk_pilihan, pembeliBaru);
			    	
				} else if ( input2_int == 0 ) {
			    	k='y';
				} else {
					k = 'y';
					BeliProduk(kategori, input2_int, pembeliBaru);
				}
			}
			
		} else if ( input == "2"){
			while(1){
				system("cls");
				gotoxy(23, 10); cout << "+" << string(70, '=') << "+" << endl;
				gotoxy(23, 11); cout << "|                             URUTKAN DATA                             |" << endl;	
				gotoxy(23, 12); cout << "|----------------------------------------------------------------------|" << endl;	
				gotoxy(23, 13); cout << "| 1. Harga terkecil ke terbesar                                        |" << endl;
				gotoxy(23, 14); cout << "| 2. Harga terbesar ke terkecil                                        |" << endl;
				gotoxy(23, 15); cout << "+" << string(70, '=') << "+" << endl;
				gotoxy(23, 16); cout << " Pilih jenis pengurutan data: ";
				cin >> jenis_sort;
				if ( jenis_sort == "1" || jenis_sort == "2" ){
					break;
				}
			}
			int urutan = atoi(jenis_sort.c_str());
			if (kategori == "makanan") {
				produk tempData [jumlah_produk_pilihan];
				sortirProduk( &HEAD_Makanan, 		&TAIL_Makanan, 		tempData, 	jumlah_produk_pilihan, 	"harga", urutan, &sortNama_Makanan);
				sortNama_Makanan = 0;
			} else if (kategori == "minuman") {
				produk tempData [jumlah_produk_pilihan];
				sortirProduk( &HEAD_Minuman, 		&TAIL_Minuman,		tempData, 	jumlah_produk_pilihan,	"harga", urutan, &sortNama_Minuman);
				sortNama_Minuman = 0;
			} else if (kategori == "obat") {
				produk tempData [jumlah_produk_pilihan];
				sortirProduk( &HEAD_Obat, 			&TAIL_Obat,			tempData, 	jumlah_produk_pilihan,	"harga", urutan, &sortNama_Obat);
				sortNama_Obat = 0;
			} else if (kategori == "atk") {
				produk tempData [jumlah_produk_pilihan];
				sortirProduk( &HEAD_ATK, 			&TAIL_ATK,			tempData, 	jumlah_produk_pilihan,	"harga", urutan, &sortNama_ATK);
				sortNama_ATK = 0;
			} else if (kategori == "perabot") {
				produk tempData [jumlah_produk_pilihan];
				sortirProduk( &HEAD_Perabot, 		&TAIL_Perabot, 		tempData, 	jumlah_produk_pilihan,	"harga", urutan, &sortNama_Perabot);
				sortNama_Perabot = 0;
			} else if (kategori == "elektronik") {
				produk tempData [jumlah_produk_pilihan];
				sortirProduk( &HEAD_Elektronik, 	&TAIL_Elektronik, 	tempData, 	jumlah_produk_pilihan,	"harga", urutan, &sortNama_Elektronik);
				sortNama_Elektronik = 0;
			} else if (kategori == "peralatanbayi") {
				produk tempData [jumlah_produk_pilihan];
				sortirProduk( &HEAD_PeralatanBayi,&TAIL_PeralatanBayi,	tempData, 	jumlah_produk_pilihan, 	"harga", urutan, &sortNama_PltBayi);
				sortNama_PltBayi = 0;
			}
			
			PilihProduk(kategori, jumlah_produk_pilihan, pembeliBaru);
		} else if ( input != "0"){
			Message(1);
			sleep(2);
		}
	
	}
}

void MenuPembeli(pembeli* pembeliBaru){	
	char keluar='n';
	while(keluar=='n'){
		
		//Cek apakah no. antrian berubah
		//Jika no. antrian berubah ( habis checkout) pembeli tak perlu masuk ke menu pembeli lagi
		if (CURR_Antrian_TEMP != CURR_Antrian){
			CURR_Antrian_TEMP = CURR_Antrian;
			keluar = 'y';
		}
		system("cls");
		hiasan();
		
		string input; int x_pos = 26;
		gotoxy(x_pos, 3);  cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(x_pos, 4);  cout << "|                             MENU PEMBELI                             |" << endl;
		gotoxy(x_pos, 5);  cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(x_pos, 6);  cout << "| 1. Makanan                                                           |" << endl;
		gotoxy(x_pos, 7);  cout << "| 2. Minuman                                                           |" << endl;
		gotoxy(x_pos, 8);  cout << "| 3. Obat-obatan                                                       |" << endl;
		gotoxy(x_pos, 9);  cout << "| 4. Alat Tulis Kerja                                                  |" << endl;
		gotoxy(x_pos, 10); cout << "| 5. Perabotan Rumah                                                   |" << endl;
		gotoxy(x_pos, 11); cout << "| 6. Alat / Perkakas Elektronik                                        |" << endl;
		gotoxy(x_pos, 12); cout << "| 7. Peralatan Bayi                                                    |" << endl;
		gotoxy(x_pos, 13); cout << "|----------------------------------------------------------------------|" << endl;
		gotoxy(x_pos, 14); cout << "| 99. Keranjang belanja                                                |" << endl;
		gotoxy(x_pos, 15); cout << "|----------------------------------------------------------------------|" << endl;
		gotoxy(x_pos, 16); cout << "| 0. Keluar dari menu pembeli                                          |" << endl;
		gotoxy(x_pos, 17); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(x_pos, 18); cout << " Masukan pilihan anda : "; cin >> input;
		fflush(stdin);
		if (input == "1"){
			PilihProduk("makanan", length(HEAD_Makanan), pembeliBaru);
			
		} else if (input == "2"){
			PilihProduk("minuman", length(HEAD_Minuman), pembeliBaru);
			
		} else if (input == "3"){
			PilihProduk("obat", length(HEAD_Obat), pembeliBaru);
			
		} else if (input == "4"){
			PilihProduk("atk", length(HEAD_ATK), pembeliBaru);
			
		} else if (input == "5"){
			PilihProduk("perabot", length(HEAD_Perabot), pembeliBaru);
			
		} else if (input == "6"){
			PilihProduk("elektronik", length(HEAD_Elektronik), pembeliBaru);
			
		} else if (input == "7"){
			PilihProduk("peralatanbayi", length(HEAD_PeralatanBayi), pembeliBaru);
			
		} else if (input == "99"){
			KeranjangBelanja(pembeliBaru);
			
		}else if (input == "0"){
			string confirm = "t";
			while (confirm == "t"){
				gotoxy(x_pos, 20); cout << "+" << string(70, '=') << "+" << endl;
				gotoxy(x_pos, 21);  cout << "|                              KONFIRMASI                              |" << endl;
				gotoxy(x_pos, 22);  cout << "|     KELUAR DARI MENU PEMBELI BERARTI SEGALA PEMBELIAN DIBATALKAN     |" << endl;
				gotoxy(x_pos, 23);  cout << "+" << string(70, '=') << "+" << endl;
				gotoxy(x_pos, 24); cout << "  Konfirmasi keluar? (y/t) ";  cin >> confirm;
				if (confirm == "y" || confirm=="Y"){
					confirm = "y";
					TambahStokKembali(pembeliBaru);
					pembeliBaru->HeadKrj = NULL;
					pembeliBaru = NULL;
					Message(3);
					sleep(2);
					keluar='y';
				} else if (confirm != "t" || confirm != "T"){
					confirm = "y";
				} else {
					Message(1);
					sleep(2);
				}
			}

		}else {
			Message(1);
			sleep(2);
		}
	}
	MainMenu();
}


void MenuAdmin(){
	char keluar='n';
	while(keluar=='n'){
		system("cls");
		hiasan();
		
		string input; int x_pos = 26;
		gotoxy(x_pos, 2); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(x_pos, 3); cout << "|                                ADMIN                                 |" << endl;
		gotoxy(x_pos, 4); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(x_pos, 5); cout << "|  1. Data produk                                                      |" << endl;
		gotoxy(x_pos, 6); cout << "|  2. Riwayat pembelian per- 7 hari                                    |" << endl;
		gotoxy(x_pos, 7); cout << "|  3. Manajemen Antrian                                                |" << endl;
		gotoxy(x_pos, 8); cout << "|  4. Logout                                                           |" << endl;
		gotoxy(x_pos, 9); cout << "|  5. Keluar dari sistem                                               |" << endl;
		gotoxy(x_pos, 10); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(x_pos, 11); cout << " Masukan pilihan anda : "; cin >> input;
		fflush(stdin);
		if (input == "1"){
			DataProduk();
			
		} else if (input == "2"){
			RiwayatOrAntrianPembeli(TAIL_RiwayatPembeli);
			
		} else if (input == "3"){
			ManajemenAntrianPembeli();
			
		}else if (input == "4"){
			keluar='y';
			SaveDataOnExit();
			MainMenu();
			
		} else if (input == "5"){
			keluar='y';
			Loading("exit");
			Message(99);
			sleep(2);
		} else {
			Message(1);
			sleep(2);
		}
	}
}


int kesempatan=3;
bool login = false;

void LoginAdmin(){	
	
	while (login == false){	
		string id, pass;
		system ("cls");
		
		gotoxy(40, 3); cout <<"+-----------------------------------------------+" << endl;
		gotoxy(88, 4); cout <<"|"; gotoxy(40, 4); cout <<"| Kesempatan login :  " << kesempatan << endl;
		gotoxy(40, 5); cout <<"|-----------------------------------------------|"<< endl;
		
		gotoxy(40, 8); cout <<"+-----------------------------------------------+"<< endl;
		gotoxy(86, 6); cout <<"] |"; gotoxy(40, 6); cout <<"| Masukkan Username : [ "; cin >> id;
		gotoxy(86, 7); cout <<"] |"; gotoxy(40, 7); cout <<"| Masukkan Password : [ "; cin >> pass;
		
		Loading("login");
		
		if( id=="admin" && pass=="admin" ){
			login = true ;
			
			Message(90);
			sleep(2);
			MenuAdmin();
			
			
		}else{
			kesempatan-=1;
			
			Message(88);
			sleep(2);
		}
		
		if(kesempatan == 0){
			Message(89);
			sleep(2);
			MainMenu();
		}
	}
}

void MainMenu(){
	char keluar='n';
	while(keluar=='n'){
		system("cls");
		string input;
		
		Opening();
		system("cls");
		system("color 17");
		gotoxy(26, 3); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(26, 4); cout << "|        Selamat Datang di Aplikasi Konter HP Team Secret [TS]         |" << endl;
		gotoxy(26, 5); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(26, 6); cout << "|  1. Admin                                                            |" << endl;
		gotoxy(26, 7); cout << "|  2. Pembeli                                                          |" << endl;
		gotoxy(26, 8); cout << "+" << string(70, '=') << "+" << endl;
		gotoxy(26, 9); cout << " Siapakah anda : "; cin >> input;
		fflush(stdin);
		if (input == "1"){
			if ( kesempatan > 0){
				keluar = 'y';
				login = false;
				LoginAdmin();
			} else {
				Message(89);
				sleep(2);
				keluar = 'y';
				MainMenu();
			}
			
			
		} else if (input == "2"){
			keluar = 'y';
			pembeli* pembeliBaru = new pembeli();
			MenuPembeli(pembeliBaru);
			
		}  else {
			Message(1);
			sleep(2);
		}
	}
}



int main(){
	Loadout();
	DeteksiResetRiwayat();
	MainMenu();
	
	//MenuAdmin();
	
	return 0;
}

